# Bob
This plugin allows players to run commands as easily as sending a chat message.

## Config
The config is used to register commands with chat messages.
<br>%p will be replaced with the player's name.
<br>%mp will be replaced by the text in message-prefix.
<br>All console commands support chatcolors.
<br>Any number of Bob commands may be added.
<br>Any number of console commands may be added to a Bob command.
<br>Commands will be run as console.

Here is the default config for reference:
<pre>
message-prefix: '&c[&6BOB&c]&r'

commands:
  'give me creative':
    - 'gamemode creative %p'
    - 'tell %p %mp Your &2gamemode&r was set to &6creative&r.'
  'give me survival':
    - 'gamemode survival %p'
    - 'tell %p %mp Your &2gamemode&r was set to &6survival&r.'
</pre>

## Permissions
<pre>bob.use</pre>
is the only permission.
<br>It allows players to use the plugin.
<br>By default, only op's have this permission.

## Commands
This plugin does not have any commands of its own.
