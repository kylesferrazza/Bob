package io.github.tubakyle.bob;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;
import org.bukkit.plugin.java.JavaPlugin;

/**
 * Created by Kyle Sferrazza on 8/9/2014.
 * This file is a part of: Bob.
 * All rights reserved.
 */
public class Bob extends JavaPlugin implements Listener {
    @Override
    public void onEnable() {
        saveDefaultConfig();
        getServer().getPluginManager().registerEvents(this, this);
    }

    @EventHandler
    public void onChat (AsyncPlayerChatEvent event) {
        getLogger().info("player chatted.");
        Player player = event.getPlayer();
        String message = event.getMessage();
        reloadConfig();
        getLogger().info("message: " + message);
        String path = "commands." + message;
        getLogger().info("path: " + path);
        if (getConfig().isSet(path)) {
            getLogger().info("path is set");
            for (String command : getConfig().getStringList(path)) {
                getLogger().info("command: " + command);
                doCommand(command, player);
                event.setCancelled(true);
            }
        }

    }

    public void doCommand(String command, Player player) {
        getLogger().info("doing command.");
        command = command.replace("%p", player.getName());
        command = command.replace("%mp", getConfig().getString("message-prefix"));
        command = ChatColor.translateAlternateColorCodes('&', command);
        CommandSender console = Bukkit.getConsoleSender();
        Bukkit.dispatchCommand(console, command);
    }
}
